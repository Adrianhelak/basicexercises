package obiektowe9;

public class Car {

    private String name;//
    private int seats;//
    private int tank;//
    private int hp;//
    private int currentSpeed;
    private int mileage;
    private int year;
    private int passager = 0;
    private int oIle;
    private double maxSpeed;

    public void maxSpeed() {
        if (hp < 200) {
            maxSpeed = 1.2 * hp;
        } else {
            maxSpeed = hp;
        }
    }

    public void getCarName(String name, int year) {
        System.out.println("Marka " + name + " rok produkcji " + year);
        this.name = name;
        this.year = year;
    }

    public void addPassager() {
        if (seats > passager) {
            passager++;
        } else {
            System.err.println("Nie mozna dodac pasazera");
        }
    }

    public void delPassager() {
        if (passager == 0) {
            System.err.println("Ilosc pasazerow juz jest 0");
        } else {
            passager--;
        }
    }


    public void speedIncrease(int oIle) {
        if (passager > 0) {
            if (currentSpeed + oIle > maxSpeed) {
                System.out.println("Osiagnieto predkosc maksymalna " + maxSpeed);
            } else {
                this.currentSpeed += oIle;
            }
        } else System.out.println("Nie mozna przyspieszczyc bez kierowcy");

    }

    public void decreaseIncrease(int oIle) {
        if (passager > 0) {
            if (currentSpeed > oIle) {
                currentSpeed -= oIle;
            } else {
                System.out.println("Samochod sie zatrzyma");
                currentSpeed = 0;
            }
        } else {
            System.out.println("Nie mozna sie zatrzymac bez kierowcy");
        }
    }
}