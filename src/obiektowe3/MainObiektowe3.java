package obiektowe3;

public class MainObiektowe3 extends Dziennik {
    public static void main(String[] args) {
        Dziennik uczen = new Dziennik();
        uczen.addStudent("Krzysztof Grrec");
        uczen.addStudent("Milena Koj");
        uczen.addStudent("Anrzej Andrejowic");
        uczen.wypStudent();

        System.out.println();
        uczen.delStudent();
        uczen.wypStudent();
        System.out.println();

        uczen.addStudent("Swiety Mikolaj");
        uczen.wypStudent();

    }
}
