package zadanie33;

import java.util.Scanner;

public class MainZadanie33 {
    public static void main(String[] args) {
        System.out.println("Wpisz slowo ");
        Scanner konsola = new Scanner(System.in);
        String slowo = konsola.nextLine();
        int x = 0;
        int y = 0;
        for (int i = 0; i < slowo.length(); i++) {
            if (slowo.charAt(i) == '(') {
                x++;
            }
            if (slowo.charAt(i) == ')') {
                y++;
            }
            if (x < y) {
                System.out.println("Nawiasy sa zle sprawane");
                break;
            }
        }
    }
}
