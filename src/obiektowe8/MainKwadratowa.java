package obiektowe8;

import java.util.Scanner;

public class MainKwadratowa {
    public static void main(String[] args) {
        QuadraticEquation rownieDelta = new QuadraticEquation();
        Scanner sq = new Scanner(System.in);
        System.out.println("Wpisz a");
        rownieDelta.setA(sq.nextInt());
        System.out.println("Wpisz b");
        rownieDelta.setB(sq.nextInt());
        System.out.println("Wpisz c");
        rownieDelta.setC(sq.nextInt());

        System.out.println(rownieDelta.calculateDelta());

        System.out.println(rownieDelta.calculateX1());
        System.out.println(rownieDelta.calculateX2());

    }
}
