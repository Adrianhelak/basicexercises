package obiektowe8;

public class QuadraticEquation {
    private int a;
    private int b;
    private int c;

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public double calculateDelta() {
        double rownanie = (b * b) - (4 * a * c);
        double delta = Math.sqrt(rownanie);
        return delta;
    }

    public double calculateX1() {
        double x1 =((-b)-calculateDelta())/(2*a);
        return x1;
    }

    public double calculateX2() {
        double x2 =((-b)+calculateDelta())/(2*a);
        return x2;
    }
}
