package zadanie10;

import java.text.DecimalFormat;
import java.util.Scanner;

public class MainZadanie10 {
    public static void main(String[] args) {
        /*
        Napisać program obliczający należny podatek dochodowy od osób ﬁzycznych. Program ma pobierać od użytkownika dochód i po obliczeniu wypisywać na ekranie należny podatek. Podatek obliczany jest wg. następujących reguł:
	• do 85.528 podatek wynosi 18% podstawy minus 556,02 PLN,
	• od 85.528 podatek wynosi 14.839,02 zł + 32% nadwyżki ponad 85.528,00
	*/
        Scanner dochodIn = new Scanner(System.in);
        System.out.print("Podaj dochod ");
        double dochod = dochodIn.nextDouble();


        double podatek;
        if(dochod<= 85528){
            podatek= (dochod * 0.18)-556.02;
        } else podatek = 14839.02 + ((dochod-85528)* 0.32);

        DecimalFormat f = new DecimalFormat("##.00");
        System.out.println("Podatek dochodowy wynosi "+ f.format(podatek));

    }
}
