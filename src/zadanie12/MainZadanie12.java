package zadanie12;

import java.util.Scanner;

public class MainZadanie12 {
    public static void main(String[] args) {
        System.out.println("Wpisz wartos: ");
        Scanner scanner = new Scanner(System.in);
        int value = scanner.nextInt();

        for (int i = 0; i < value; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
        for (int j = 3; j <= 100; j++) {
            if (j % 3 == 0 || j % 5 == 0) {
                System.out.print(j + " ");
            }
        }
        System.out.println();
        System.out.print("Podaj dolny zakres przedzialu ");
        Scanner dol = new Scanner(System.in);
        int down = dol.nextInt();
        System.out.print("Podaj gorny zakres przedzialu ");
        Scanner gora = new Scanner(System.in);
        int up = gora.nextInt();

        for (int k = down; k <= up; k++) {
            if (k % 6 == 0) {
                System.out.print(k + " ");
            }
        }
    }
}
