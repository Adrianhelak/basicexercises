package zadanie17;

import java.util.Scanner;

public class MainZadanie17 {
    public static void main(String[] args) {
        System.out.println("Wpisz liczbe ");
        Scanner konsola = new Scanner(System.in);
        int calkowita = konsola.nextInt();

        for (int a = 1; a <= calkowita; a += a) {
            System.out.print(a + " ");
        }
    }
}
