package obiektowe12;

import java.util.Scanner;

public class MainObiektowe5V2 {
    public static void main(String[] args) {
        KoloV2 kolo = new KoloV2();
        KwadratV2 kwadrat = new KwadratV2();
        ProstokatV2 prostokat = new ProstokatV2();

        System.out.println("Wpisz figure");
        Scanner konsola = new Scanner(System.in);
        String figura = konsola.next();

        switch (figura) {
            case "kolo": {
                System.out.println("Wpisz promien ");
                int x = konsola.nextInt();
                kolo.setX(x);
                System.out.println("Wpisz komende pole/ obwod");
                String command = konsola.next();
                switch (command) {
                    case "pole": {
                        System.out.println("Pole = " + kolo.poleKolaV2());
                        break;
                    }
                    case "obwod": {
                        System.out.println("Obwod = " + kolo.obwodKolaV2());
                        break;
                    }
                }
                break;
            }
            case "kwadrat": {
                System.out.println("Wpisz a ");
                int a = konsola.nextInt();
                kwadrat.setA(a);
                System.out.println("Wpisz komende pole/ obwod");
                String command = konsola.next();
                switch (command) {
                    case "pole": {
                        System.out.println("Pole = " + kwadrat.poleKwadratV2());
                        break;
                    }
                    case "obwod": {
                        System.out.println("Obwod = " + kwadrat.obwodKwadratV2());
                        break;
                    }
                }
                break;
            }
            case "prostokat": {
                System.out.println("Wpisz a ");
                int a = konsola.nextInt();
                prostokat.setA(a);
                System.out.println("Wpisz b ");
                int b = konsola.nextInt();
                prostokat.setB(b);
                System.out.println("Wpisz komende pole/ obwod");
                String command = konsola.next();
                switch (command) {
                    case "pole": {
                        System.out.println("Pole = " + prostokat.poleProstokatV2());
                        break;
                    }
                    case "obwod": {
                        System.out.println("Obwod = " + prostokat.odwodKwadratV2());
                        break;
                    }
                }


                break;
            }
            default:
                System.err.println("Nie ma takiej figury");
                break;
        }
    }
}
