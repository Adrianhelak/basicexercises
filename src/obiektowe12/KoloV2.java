package obiektowe12;

public class KoloV2 {
    private int x;
    private static final double pi = 3.14;

    public void setX(int x) {
        this.x = x;
    }

    public KoloV2() {
        this.x = x;
    }

    public double poleKolaV2() {
        return (pi * x * x);
    }

    public double obwodKolaV2() {
        return (2 * pi * x);
    }
}
