package obiektowe5;

public class Kwadrat {
    private int a;

    public Kwadrat(int a) {
        this.a = a;
    }

    public int poleKwadrat() {
        return (a * a);
    }

    public int obwodKwadrat() {
        return (4 * a);
    }
}
