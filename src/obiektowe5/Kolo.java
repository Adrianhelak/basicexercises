package obiektowe5;

public class Kolo {
    private int r;
    private static final double pi = 3.14;

    public Kolo(int r) {
        this.r = r;
    }

    public double poleKola() {
        return (pi * r * r);
    }

    public double obwodKola() {
        return (2 * pi * r);
    }
}
