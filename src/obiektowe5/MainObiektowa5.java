package obiektowe5;

import java.util.Scanner;

public class MainObiektowa5 {
    public static void main(String[] args) {
        Kolo kolo = new Kolo();
        Kwadrat kwadrat = new Kwadrat();
        Prostokat prostokat = new Prostokat();
        System.out.println("Wpisz figure");
        Scanner konsola = new Scanner(System.in);
        String figura = konsola.next();

        switch (figura) {
            case "kolo": {
                System.out.println("Wpisz promien ");
                int r = konsola.nextInt();
                System.out.println("Pole = " + kolo.poleKola(r));
                System.out.println("Obwod = " + kolo.obwodKola(r));
                break;
            }
            case "kwadrat": {
                System.out.println("Wpisz a ");
                int a = konsola.nextInt();
                System.out.println("Pole = " + kwadrat.poleKwadrat(a));
                System.out.println("Obwod = " + kwadrat.obwodKwadrat(a));
                break;
            }
            case "prostokat": {
                System.out.println("Wpisz a ");
                int a = konsola.nextInt();
                System.out.println("Wpisz b ");
                int b = konsola.nextInt();
                System.out.println("Pole = " + prostokat.poleProstokat(a, b));
                System.out.println("Obwod = " + prostokat.odwodKwadrat(a, b));
                break;
            }
            default:
                System.err.println("Nie ma takiej figury");
                break;
        }
    }
}
