package obiektowe5;

public class Prostokat {
    private int a;
    private int b;

    public Prostokat(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int poleProstokat() {
        return (a * b);
    }

    public int odwodKwadrat() {
        return ((2 * a) + (2 * b));
    }

}
