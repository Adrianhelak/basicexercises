package zadanie31;

import java.util.Scanner;

public class MainZadanie31 {
    public static void main(String[] args) {
        System.out.println("Wpisz slowo ");
        Scanner konsola = new Scanner(System.in);
        String slowo = konsola.next();

        for (int i = 0; i < slowo.length(); i++) {
            Character pZnak = slowo.charAt(i);
            Character oZnak = slowo.charAt(slowo.length() - i - 1);
            if (pZnak != oZnak) {
                System.out.println("Nie jest palindromem");
                break;
            } else {
                System.out.println("Jest palindromem");
                break;
            }
        }
    }
}
