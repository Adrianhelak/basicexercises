package zadanie15;

import java.util.Scanner;

public class MainZadanie15 {
    public static void main(String[] args) {

        System.out.println("Jestem kalkulatorem ");
        Scanner konsola = new Scanner(System.in);
        System.out.println("Wprowadz a ");
        int a = konsola.nextInt();

        System.out.println("Wprowadz znak dzialanaia");
        String x = konsola.next();

        System.out.println("Wprowadz b");
        int b = konsola.nextInt();

        while (x.equals("/") && b == 0) {
            System.err.println("Nie mozna dzielic przez zero");
            System.out.println("Wpisz inne b");
            b = konsola.nextInt();
        }

        int dodawanie = a + b;
        int odejowanie = a - b;
        int mnozenie = a * b;
        double dzielenie = a / b;

        switch (x) {
            case "+":
                System.out.println(dodawanie);
                break;
            case "-":
                System.out.println(odejowanie);
                break;
            case "/":
                System.out.println(dzielenie);
                break;
            case "*":
                System.out.println(mnozenie);
                break;
        }
    }
}
