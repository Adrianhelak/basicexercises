package zadanie22;

import java.util.Scanner;

public class MainZadanie22 {
    public static void main(String[] args) {
        System.out.println("Wpisz liczbe ");
        Scanner konsola = new Scanner(System.in);
        int n = konsola.nextInt();

        System.out.println("Dzielniki liczby to ");
        for (int i = 1; i <= n; i++) {
            if (n % i == 0) {
                System.out.print(i + " ");
            }
        }
    }
}
