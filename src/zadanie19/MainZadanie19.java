package zadanie19;

import java.util.Scanner;

public class MainZadanie19 {
    public static void main(String[] args) {
        System.out.println("Wpisz liczbe: ");
        Scanner konsola = new Scanner(System.in);
        int x = konsola.nextInt();
        int min = x;
        int max = x;
        while (x != 0) {
            if (x > max) {
                max = x;
            }
            if (x < min) {
                min = x;
            }
            x = konsola.nextInt();
        }

        System.out.println("Suma najwiekszej i najmniejszej liczby " + (min + max));
    }
}
