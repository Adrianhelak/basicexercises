package zadanie36;

import java.util.Scanner;

public class MainZadanie36 {
    public static void main(String[] args) {
        Scanner konsola = new Scanner(System.in);
        System.out.println("Wpisz a");
        int a = konsola.nextInt();
        System.out.println("Wpisz b");
        int b = konsola.nextInt();
        System.out.println("Wpisz c");
        int c = konsola.nextInt();

        int delta = (b * b) - 4 * a * c;

        if (Math.sqrt(delta) < 0) {
            System.out.println("Nie ma pierwiastkow");
        } else {
            if (Math.sqrt(delta) == 0) {
                double x0 = -b / (2 * a);
                System.out.println("Ma jeden pierwiastek " + x0);
            } else {
                double x1 = (-b - Math.sqrt(delta)) / (2 * a);
                double x2 = (-b + Math.sqrt(delta)) / (2 * a);
                System.out.println("Ma dwa pierwiastki " + x1 + " i " + x2);
            }
        }


    }
}
