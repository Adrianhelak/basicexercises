package zadanie23;

import java.util.Scanner;

public class MainZadanie23 {
    public static void main(String[] args) {
        System.out.println("Wpisz liczbe ktora bedzie sprawdzana czy jest liczba pierwsza ");
        Scanner konsola = new Scanner(System.in);
        int n = konsola.nextInt();

        int x = 0;

        for (int i = 1; i <= n; i++) {
            if (n % i == 0) {
                x++;
            }
        }
        if (x > 2) {
            System.out.println("Liczba " + n + " nie jest liczba pierwsza");
        } else System.out.println("Liczba " + n + " jest liczba pierwsza");

    }
}
