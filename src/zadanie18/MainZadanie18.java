package zadanie18;

import java.util.Scanner;

public class MainZadanie18 {
    public static void main(String[] args) {
        Scanner kosnola = new Scanner(System.in);
        System.out.println("Pobieram N liczb ");
        int n = kosnola.nextInt();
        int[] array = new int[n];

        int najmniejsza;
        int suma = 0;

        System.out.println("Wprowadz " + n + " liczb");
        for (int i = 0; i < n; i++) {
            array[i] = kosnola.nextInt();
        }
        int min = array[0];
        int max = array[0];
        for (int i = 0; i < n; i++) {
            if (array[i] < min) {
                min = array[i];
            }
            if (array[i] > max) {
                max = array[i];
            }
            suma += array[i];
        }
        int sumaAZ = min + max;
        double srednia = suma / n;

        System.out.println("Suma najmniejszej i najwiekszej liczby " + sumaAZ);
        System.out.println("Srednia arytmetyczna " + srednia);

    }
}
