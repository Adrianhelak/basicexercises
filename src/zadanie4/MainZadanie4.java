package zadanie4;

public class MainZadanie4 {
    public static void main(String[] args) {

        boolean jest_ciepło = false;
        boolean wieje_wiatr = true;
        boolean swieci_slonce = false;

        boolean ubieram_sie_cieplo = false;
        if (jest_ciepło == false && wieje_wiatr == true) {
            ubieram_sie_cieplo = true;
        }

        boolean biore_parasol = false;
        if (swieci_slonce == false && wieje_wiatr == false) {
            biore_parasol = true;
        }

        boolean ubieram_kurtke = false;
        if (jest_ciepło == false && wieje_wiatr == true && swieci_slonce == false) {
            ubieram_kurtke = true;
        }

        System.out.println("Ubieram sie cieplo: "+ubieram_sie_cieplo);
        System.out.println("Biore parasol: "+biore_parasol);
        System.out.println("Ubieram kurtke: "+ubieram_kurtke);
    }
}
