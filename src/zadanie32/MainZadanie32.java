package zadanie32;

import java.util.Scanner;

public class MainZadanie32 {
    public static void main(String[] args) {
        System.out.println("Wpisz slowo ");
        Scanner konsola = new Scanner(System.in);
        String slowo = konsola.nextLine();
        int wynik = 0;
        for (int i = 0; i < slowo.length(); i++) {
            if (slowo.charAt(i) >= '0' && slowo.charAt(i) <= '9') {
                int value = slowo.charAt(i) - '0';
                wynik += value;
            }
        }
        System.out.println("Wynik " + wynik);
    }
}
