package zadanie27;

import java.util.Scanner;

public class MainZadanie27 {
    public static void main(String[] args) {
        System.out.println("Wpisz liczbe");
        Scanner konsola = new Scanner(System.in);
        int n = konsola.nextInt();

        for (int i = 1; i <= n; i++) {
            System.out.println(i + " * " + n + " = " + (i * n));
        }
    }
}
