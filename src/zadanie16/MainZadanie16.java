package zadanie16;

import java.util.Scanner;

public class MainZadanie16 {
    public static void main(String[] args) {
        Scanner konsola = new Scanner(System.in);
        int x = konsola.nextInt();

        while (x != 0) {
            System.out.println("Hello World!");
            x = konsola.nextInt();
        }
    }
}
