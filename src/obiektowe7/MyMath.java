package obiektowe7;

public class MyMath {
    private int a;
    private int b;

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int absI(int a) {
        if (a > 0) {
            return a;
        } else {
            return a -= 2 * a;
        }
    }

    public double absD(double a) {
        if (a > 0) {
            return a;
        } else {
            return a -= 2 * a;
        }
    }

    public int powI(int a, int b) {
        for (int i = 1; i <= b; i++) {
            a *= a;
        }
        return a;
    }

    public double powD(double a, double b) {
        for (int i = 1; i <= b; i++) {
            a *= a;
        }
        return a;
    }

}
