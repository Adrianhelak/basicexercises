package obiektowe11;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainArray {
    public static void main(String[] args) {
//        Array array = new Array();
//        Scanner sc = new Scanner(System.in);
//        System.out.println("ustaw n");
//        int n = sc.nextInt();
//        array.setArray(n);

        List<Integer> list = new ArrayList<>();
        list.add(2);
        list.add(5);

        //
        list.get(0);// 2
        list.get(1);// 5
        list.get(2);// ArrayIndexOutOfBoundsException

        list.remove(0); // usuwa 2


    }

    public static double obliczSume(List<Integer> list) {
        int suma = 0;
        for (int i : list
                ) {
            suma += list.get(i);
        }
        return suma;
    }

    public static double obliczIloczyn(List<Integer> list) {
        int iloczyn = 1;
        for (int i : list
                ) {
            iloczyn *= list.get(i);
        }
        return iloczyn;
    }
    public static double srednia(List<Integer> list){
//        int srednia=0;
//        for (int i: list
//             ) {
//            srednia+=list.get(i);
//        }
        return (obliczSume(list)/list.size());
    }
}
