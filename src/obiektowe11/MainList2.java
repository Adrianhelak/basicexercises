package obiektowe11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainList2 {
    public static void main(String[] args) {

        int[] array = new int[]{4, 2, 2, 1, 5, 29, 3, 8};
        int tablica[] = new int[array.length];
        List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 4, 2, 5, 12, 3, 2));
        List<Integer> duzaLista = new ArrayList<Integer>();
        List<Integer> lista2 = new ArrayList<>();
        List<Integer> letter = new ArrayList<Integer>();


        iloscRowne(list, array);
        kopiujeListe(list);
        listaDoTablicy(list, tablica);
        tablicaDoListy(list,lista2,tablica);
        dwieListyDoJednej(list,lista2);
        sprawdzCzySaTakieSame(list,lista2);
        System.out.println(duzaLista);

    }

//    private static int[] array = new int[]{4, 2, 2, 1, 5, 29, 3,b 8};
//    private static List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 4, 2, 5, 12, 3, 2));
//    private static List<Integer> lista2 = new ArrayList<>();
//    private static int[] tablica;

    public static void kopiujeListe(List<Integer> list) {
        List<Integer> letter = new ArrayList<Integer>(list);

        //TODO: ale czemu nie zwraca ?
    }

    public static void listaDoTablicy(List<Integer> list, int[] tablica) {
        for (int i = 0; i < list.size(); i++) {
            tablica[i] = list.get(i);
        }
        //TODO: zrób opcje w ktorej tworzysz w metodzie nowa tablice i przepisujesz do niej elementy, a nastepnie ta tablice
        // zwracasz
    }

    public static void iloscRowne(List<Integer> list, int[] array) {
        int count = 0;
        for (int i = 0; i < list.size(); i++) {
            if (array[i] == list.get(i)) {
                count++;
            }
        }
        System.out.println("Ilosc rownych " + count);
        // TODO: zwroc ilosc rownych elementow
    }

    public static void tablicaDoListy(List<Integer> list, List<Integer> lista2, int[] tablica) {

        for (int i = 0; i < list.size(); i++) {
            lista2.add(tablica[i]);
        }
        // TODO: na odwrot z lista do tablicy. stworz nowa liste, wypelnij ja elementami tablicy i zwroc nowa liste
        //TODO: czemu sa dwie listy w parametrach
        // TODO: iterujesz ilosc elementow : list.size(), a dodajesz do listy lista2, a ilosc elementow powinna byc
        // TODO : tablica.length
    }

    public static void dwieListyDoJednej(List<Integer> list, List<Integer> lista2) {
        List<Integer> duzaLista = new ArrayList<Integer>(list);
        duzaLista.addAll(lista2);

        // TODO : zwroc jedna liste zawierajaca elementy obu list
    }

    public static void sprawdzCzySaTakieSame(List<Integer> list, List<Integer> lista2) {
        if (list.equals(lista2) == true) {
            System.out.println("Prawda");
        } else System.out.println("Nie prawda");
        // TODO: zwroc wynik (true/false) jesli jest lub nie jest taka sama
    }

}
