package zadanie26;

import java.util.Scanner;

public class MainZadanie26 {
    public static void main(String[] args) {
        System.out.println("Wpisz n ");
        Scanner konsola = new Scanner(System.in);
        int n = konsola.nextInt();

        String napis = ""; // dodajemy zmienna napis
        for (int i = n; i > 0; ) {
            if (i % 2 != 0) {
                napis += "1"; // wpisujemy do napisu 1
                i -= 1;
                i /= 2;
            } else {
                if (i % 2 == 0) {
                    napis += "0"; // wpisujemy do napisu 0
                    i /= 2;
                } else {
                    napis += "1"; // wpisujemy do napisu 1
                }
            }
        }
        for (int i = napis.length() - 1; i >= 0; i--) {
            System.out.print(napis.charAt(i)); // wypisujemy 01 stringa w odwrotnej kolejnosci
        }
    }
}
