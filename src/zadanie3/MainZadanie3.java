package zadanie3;

public class MainZadanie3 {

    public static void main(String[] args) {
        int zmiennaA = 4;
        int zmiennaB = 6;
        int zmiennaC = 23;

        System.out.println("A: " + zmiennaA + " B: " + zmiennaB + " C: " + zmiennaC);
        int zmiennaTymczasowa;

        zmiennaTymczasowa = zmiennaA;
        zmiennaA = zmiennaB;
        zmiennaB = zmiennaC;
        zmiennaC = zmiennaTymczasowa;

        System.out.println("A: " + zmiennaA + " B: " + zmiennaB + " C: " + zmiennaC);

    }
}
