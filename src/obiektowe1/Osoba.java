package obiektowe1;

public class Osoba {

    public Osoba(String name, int wiek) {
        this.name = name;
        this.wiek = wiek;
    }

    private String name;
    private int wiek;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }


    @Override
    public String toString() {
        return "Jestem " + name +
                ", mam " + wiek +
                " lat.";
    }

}

