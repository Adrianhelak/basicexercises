package obiektowe1;

public class MainObiektowa1 {
    public static void main(String[] args) {

        Osoba osoba1 = new Osoba("Kamil", 25);
        Osoba osoba2 = new Osoba("Szymon", 32);
        Osoba osoba3 = new Osoba("Mateusz", 27);

        System.out.println(osoba1); //domyslnie ustawia toString
        System.out.println(osoba2); //domyslnie ustawione toString
        System.out.println(osoba3.toString());

        System.out.println("Jestem " + osoba1.getName() + " i mam " + osoba1.getWiek() + ".");
        System.out.println("Jestem " + osoba2.getName() + " i mam " + osoba2.getWiek() + ".");
        System.out.println("Jestem " + osoba3.getName() + " i mam " + osoba3.getWiek() + ".");
    }
}
