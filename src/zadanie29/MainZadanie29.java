package zadanie29;

import java.util.Scanner;

public class MainZadanie29 {
    public static void main(String[] args) {
        System.out.println("Wpisz slow ");
        Scanner konsola = new Scanner(System.in);
        String napis = konsola.next();

        int count = 0;
        Character oLitera = napis.charAt(napis.length() - 1);

        for (int i = napis.length(); i > 0; i--) {
            Character oZnak = napis.charAt(napis.length() - i);
            if (oZnak == oLitera) {
                count++;
            }
        }
        System.out.println("Ilosc znaku" + oLitera + " to " + count);
    }
}
