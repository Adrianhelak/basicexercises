package zadanie28;

import java.util.Scanner;

public class MainZadanie28 {
    public static void main(String[] args) {
        System.out.println("Wpisz liczbe ");
        Scanner konsola = new Scanner(System.in);
        int n = konsola.nextInt();

        boolean[][] array = new boolean[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                array[i][j] = true;// ze nie maja wspolnego dzielnika
                for (int k = 2; k <= n; k++) {
                    if ((((i + 1) % k) == 0) && (((j + 1) % k) == 0)) {
                        // jesli maja jakikolwiek wspolny dzielnik
                        array[i][j] = false;
                    }
                }
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (array[i][j]) {
                    System.out.print("+ ");
                } else {
                    System.out.print(". ");
                }
            }
            System.out.println();
        }
    }
}
