package obiektowe10;

public class Field {
    private boolean[][] pole;

    public void setPole(int n) {
        this.pole = new boolean[n][n];
    }

    public void printField() {
        for (int i = 0; i < pole.length; i++) {
            for (int j = 0; j < pole[i].length; j++) {
                if (pole[i][j] == true) {
                    System.out.print("X ");
                } else {
                    System.out.print("O ");
                }
            }
            System.out.println();
        }
    }

    public void checkCell(int a, int b) {

        if (pole[a][b] == true) {
            System.out.println("komorka byla sprawdzana");
        } else {
            pole[a][b] = true;
            System.out.println("X ");
        }
    }

}
