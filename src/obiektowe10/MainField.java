package obiektowe10;

import java.util.Scanner;

public class MainField {
    public static void main(String[] args) {
        Field field = new Field();
        Scanner pole = new Scanner(System.in);
        System.out.println("Wielkosc ");
        int n = pole.nextInt();
        field.setPole(n);

        int a, b;
        int c = 0;


        boolean isWorking = true;
        while (isWorking) {
            System.out.println("komenda: check / print / quit");
            String comand = pole.next();
            switch (comand) {
                case "check": {
                    System.out.println("a");
                    a = pole.nextInt();
                    System.out.println("b");
                    b = pole.nextInt();
                    field.checkCell(a, b);
                    break;
                }
                case "print": {
                    field.printField();
                    break;
                }
                case "quit": {
                    isWorking = false;
                    break;
                }
                default:
                    System.err.println("Nie ma takiej komendy");
                    comand = pole.next();
                    break;
            }
        }
    }

}