package obiektowe13;

public class Main {
    public static void main(String[] args) {
        Student student = new Student("a", "b", 123);

        student.addGrade(5);
        student.addGrade(4);
        student.addGrade(5);
        student.addGrade(4);
        student.addGrade(3);
        student.addGrade(4);
        student.addGrade(5);


        System.out.println(student.didPass());
        System.out.println(student.getAverage());

        student.addGrade(1);

        System.out.println(student.didPass());
        System.out.println(student.getAverage());
    }
}
