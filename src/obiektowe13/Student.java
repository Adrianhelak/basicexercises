package obiektowe13;

import java.util.ArrayList;
import java.util.List;

public class Student {
    /*
      - pole numer indeksu
        - pole imie
        - pole nazwisko
        - listę ocen
     */
    private int index;
    private String name;
    private String surname;
    private List<Integer> grades = new ArrayList<>();

    public Student(String name, String surname, int index) {
        this.index = index;
        this.name = name;
        this.surname = surname;
    }

    public boolean didPass() {
        for (Integer grade : grades
                ) {
            if (grade <= 2) {
                return false;
            }
        }
        return true;
    }

    public double getAverage() {
        if (grades.size() == 0) {
            throw new ArithmeticException("Brak ocen!");
        }
        double sum = 0.0;
        for (Integer grade : grades
                ) {
            sum += grade;
        }
        return sum/grades.size();
    }

    public void addGrade(int grade) {
        grades.add(grade);
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
