package zadanie20;

import java.util.Scanner;

public class MainZadanie20 {
    public static void main(String[] args) {
        int random = (int) (Math.random() * 100 + 1);
        //System.out.println(random);

        Scanner konsola = new Scanner(System.in);
        int wpisana = konsola.nextInt();

        while (wpisana != random) {
            if (wpisana < random) {
                System.out.println("Za malo :( ");
                wpisana = konsola.nextInt();
            }
            if (wpisana > random) {
                System.out.println("Za DUZO : ");
                wpisana = konsola.nextInt();
            }
        }
        System.out.println("Brawo wygrales");
    }
}
