package zadanie34;

import java.util.Scanner;

public class MainZadanie34 {
    public static void main(String[] args) {
        System.out.println("Wpisz ");
        Scanner konsola = new Scanner(System.in);
        String msg = konsola.nextLine();
        System.out.println("Ilosc przesuniecia");
        int shift = konsola.nextInt();

        String s = "";
        int len = msg.length();
        for (int x = 0; x < len; x++) {
            char c = (char) (msg.charAt(x) + shift);
            if (c > 'z')
                s += (char) (msg.charAt(x) - (26 - shift));
            else
                s += (char) (msg.charAt(x) + shift);
        }
        System.out.println(s);
    }
}

