package zadanie35;

import java.util.Scanner;

public class MainZadanie35 {
    public static void main(String[] args) {
        Scanner konsola = new Scanner(System.in);
        System.out.println("Wpisz a ");
        int a = konsola.nextInt();
        System.out.println("Wpisz b ");
        int b = konsola.nextInt();

        try {
            if (b <= 0) {
                throw new NumberFormatException();
            } else System.out.println("Wynik dzielenia" + a / b);

        }catch (NumberFormatException nfe){
            System.err.println("B jest ujemne");
        }


    }
}
