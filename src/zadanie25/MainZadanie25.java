package zadanie25;

public class MainZadanie25 {
    public static void main(String[] args) {
        int[] array = new int[20];
        for (int i = 0; i < array.length; i++) {
            int random = (int) (Math.random() * 10);
            random += 1;
            array[i] = random;
            System.out.print(array[i] + " ");
        }
        System.out.println();
        for (int i = 1; i <= 10; i++) {
            int ilosc = 0;
            for (int j = 0; j < array.length; j++) {
                if (i == array[j]) {
                    ilosc++;
                }
            }
            System.out.println(i + "-" + ilosc);
        }
    }
}
