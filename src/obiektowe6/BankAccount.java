package obiektowe6;

public class BankAccount {
    private int saldo;

    public void addMoney(int a) {
        saldo += a;
    }

    public void substractMoney(int a) {
        saldo -= a;
    }

    public void printBankAccountStatus() {
        System.out.println("Stan konta: " + saldo);
    }
}
