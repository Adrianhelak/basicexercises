package obiektowe6;

import java.util.Scanner;

public class MainBank {
    public static void main(String[] args) {
        BankAccount account = new BankAccount();
        Scanner konsole = new Scanner(System.in);
        System.out.println("Wybierz");
        System.out.println("A: Dodaj srodki \t B:Wyjmij srodki \t C:Sprawdz srodki");
        String choice = konsole.next();
        switch (choice) {
            case "a": {
                System.out.println("ile chcesz dodac");
                int a = konsole.nextInt();
                account.addMoney(a);
                break;
            }
            case "b": {
                System.out.println("ile chcesz wyjac");
                int a = konsole.nextInt();
                account.substractMoney(a);
                break;
            }
            case "c": {
                account.printBankAccountStatus();
                break;
            }
            default: {
                System.err.println("Nie ma takiej operacji");
                break;
            }
        }
    }
}
