package zadanie24;

public class MainZadanie24 {
    public static void main(String[] args) {
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            int random = (int) (Math.random() * 20 + -10);
            array[i] = random;
            System.out.print(array[i] + " ");
        }
        int min = array[0];
        int max = array[0];

        for (int j = 1; j < array.length; j++) {
            if (array[j] > max) {
                max = array[j];
            }
            if (array[j] < min) {
                min = array[j];
            }
        }
        System.out.println();
        System.out.println("Max " + max + " Min " + min);

        int suma = 0;
        for (int k = 0; k < array.length; k++) {
            suma += array[k];
        }
        double srednia = suma / array.length;
        System.out.println("Srednia " + srednia);

        int wieksze = 0;
        int mniejsze = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > srednia) {
                wieksze++;
            }
            if (array[i] < srednia) {
                mniejsze++;
            }
        }
        System.out.println("Ilosc liczb wiekszych od sredniej " + wieksze);
        System.out.println("Ilosc liczb mniejszych od sredniej " + mniejsze);

        for (int l = array.length - 1; l >= 0; l--) {
            System.out.print(array[l] + " ");
        }
    }
}
