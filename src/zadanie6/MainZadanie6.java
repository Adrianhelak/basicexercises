package zadanie6;

public class MainZadanie6 {
    public static void main(String[] args) {

        int ocena_matematyka = 4;
        int ocena_chemia = 2;
        int ocena_j_polski = 1;
        int ocena_j_angielski = 1;
        int ocena_wos = 1;
        int ocena_informatyka = 4;

        float sredniaOcen = (ocena_matematyka + ocena_chemia + ocena_j_polski + ocena_j_angielski + ocena_wos + ocena_informatyka) / 6F;
        float sredniaOcenScislych = (ocena_matematyka + ocena_chemia + ocena_informatyka) / 3F;
        float sredniaOcenHuman = (ocena_j_angielski + ocena_j_polski + ocena_wos) / 3F;

        if (sredniaOcen < 2) {
            System.out.println("Srednia wszystkich ocen jest niedostateczna");
        } else System.out.printf("\nSrednia wszystkich ocen= %.2f" , sredniaOcen);
        if (sredniaOcenScislych < 2) {
            System.out.println("\nSrednia ocen z scislych jest niedostateczna");
        } else System.out.printf("\nSrednia ocen z scislych= %.2f " , sredniaOcenScislych);
        if (sredniaOcenHuman < 2) {
            System.out.println("\nSrednia ocen z human jest niedostateczna");
        } else System.out.printf("\nSrednia ocen z humanistycznych= %.2f " , sredniaOcenHuman);

        if (ocena_matematyka == 1) {
            System.out.println("Ocena z matematyki jest niedostateczna");
        }
        if (ocena_chemia == 1) {
            System.out.println("Ocena z chemii jest niedostateczna");
        }
        if (ocena_j_polski == 1) {
            System.out.println("Ocena z j.polskiego jest niedostateczna");
        }
        if (ocena_j_angielski == 1) {
            System.out.println("Ocena z j.angielskiego jest niedostateczna");
        }
        if (ocena_wos == 1) {
            System.out.println("Ocena z wosu jest niedostateczna");
        }
        if (ocena_informatyka == 1) {
            System.out.println("Ocena z informatyki jest niedostateczna");
        }
    }
}
